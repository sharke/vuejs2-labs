import todos from "@/store/modules/todos.js";
import users from "@/store/modules/users.js";

import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    todos,
    users,
  },
});
