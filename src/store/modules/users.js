import axios from "axios";
import { db } from "@/api/firebase-config.js";
import {
  collection,
  getDocs,
  /*addDoc,
  deleteDoc,
  doc, */
  /* updateDoc,*/
} from "firebase/firestore";
const isFirebaseSource = true;

const API_TODOS = "http://localhost:5151/users";
// json-server --watch src/assets/mocks/todos.json --port 5151

//
const usersCollectionRef = collection(db, "users");

const state = {
  users: [],
};

// GETTERS
const getters = {
  allUsers(state) {
    return state.users;
  },
};

// ACTIONS
const actions = {
  async fetchUsers({ commit }) {
    let AllUsers = []; // pour reformater en mode [{ id, name , ... }] car le format firebase ne met pas le id ds le meme niveau ..
    if (isFirebaseSource) {
      const AllUsersTmp = await getDocs(usersCollectionRef);
      AllUsersTmp.docs.map((doc) => {
        // car ds firebase les id sont ds un  niveau superieur que data , en va reecrire la liste envotée au state du store :
        AllUsers = [...AllUsers, { ...doc.data(), id: doc.id }];
      });
      commit("setUsers", AllUsers);
    } else {
      AllUsers = await axios.get(API_TODOS);
      commit("setTodos", AllUsers.data);
    }
  },
};

// MUTATIONS
const mutations = {
  setUsers(state, payload) {
    state.users = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
